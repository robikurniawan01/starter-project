module.exports = {
    halal_system_db: {
        name: process.env.halal_system_database_name,
        username: process.env.halal_system_database_user,
        password: process.env.halal_system_database_password,
        host: process.env.halal_system_database_host,
        dialect: process.env.halal_system_database_dialect,
        port: process.env.halal_system_database_port,
        logging: false,

    },
    server: {
        port: process.env.Port
    },
    security: {
        salt: process.env.halal_system_salt

    }
}