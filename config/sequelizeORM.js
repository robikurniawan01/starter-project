var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require("./config");

const Model_HalalSystem = path.join(__dirname, "../models/halal_system");
const Config_HalalSystem = new Sequelize(
    config.halal_system_db.name,
    config.halal_system_db.username,
    config.halal_system_db.password, {
        logging: config.halal_system_db.logging,
        dialect: config.halal_system_db.dialect,
        host: config.halal_system_db.host,
        port: config.halal_system_db.port,
        define: {
            timestamps: false,
            timezone: "+07:00"  
        },
        timezone: "+07:00",
        operatorsAliases: 0
    }
);
const db = {};
let model;

// Vending Machine Apps
fs.readdirSync(Model_HalalSystem)
    .filter(file => {
        return file.indexOf(".") !== 0 && file.indexOf(".map") === -1;
    })
    .forEach(file => {
        model = require(path.join(Model_HalalSystem, file))(Config_HalalSystem, Sequelize.DataTypes);
        db[model.name] = model;
    });
Object.keys(db).forEach(name => {
    if ("associate" in db[name]) {
        db[name].associate(db);
    }
});

module.exports = db