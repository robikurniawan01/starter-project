var Sequelize = require("sequelize");

const sequelizeHalalSystem = new Sequelize(process.env.halal_system_database_name, process.env.halal_system_database_user, process.env.halal_system_database_password, {
    host: process.env.halal_system_database_host,
    dialect: process.env.halal_system_database_dialect,
    port: process.env.halal_system_database_port,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    logging: false,
    timezone: "+07:00",
    operatorsAliases: 0
});


const sequelizeEmployee = new Sequelize(process.env.employee_database_name, process.env.employee_database_user, process.env.employee_database_password, {
  host: process.env.employee_database_host,
  dialect: process.env.employee_database_dialect,
  define: {
      timestamps: false,
      timezone: "+07:00"
  },
  timezone: "+07:00",
  logging: false,
  operatorsAliases: 0,
});


const sequelizeVendor = new Sequelize("AVM2", "sa", "Rider3>_<3", {
  host: "192.168.1.239",
  dialect: "mssql",
  define: {
      timestamps: false,
      timezone: "+07:00"
  },
  timezone: "+07:00",
  logging: false,
  operatorsAliases: 0,
});


const sequelizeDataSAP = new Sequelize("sap_master", "iprasetya", 'jale5253', {
  host: "192.168.1.203",
  dialect: "mysql",
  define: {
      timestamps: false,
      timezone: "+07:00"
  },
  timezone: "+07:00",
  logging: false,
  operatorsAliases: 0
});


sequelizeHalalSystem
.authenticate()
.then(() => {
  console.log('[OK] DB Halal System connected!');
})
.catch(err => {
  console.error('[ERR] DB Halal System connection error!', err);
});



sequelizeEmployee
.authenticate()
.then(() => {
  console.log('[OK] DB Employee connected!');
})
.catch(err => {
  console.error('[ERR] DB Employee connection error!', err);
});


sequelizeVendor
.authenticate()
.then(() => {
  console.log('[OK] DB Vendor connected!');
})
.catch(err => {
  console.error('[ERR] DB Vendor connection error!', err);
});


sequelizeDataSAP
.authenticate()
.then(() => {
  console.log('[OK] DB Data SAP connected!');
})
.catch(err => {
  console.error('[ERR] DB Data SAP connection error!', err);
});


module.exports = {
    sequelizeHalalSystem,
    sequelizeEmployee,
    sequelizeVendor,
    sequelizeDataSAP
};