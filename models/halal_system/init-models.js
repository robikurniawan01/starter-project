var DataTypes = require("sequelize").DataTypes;
var _mst_department = require("./mst_department");
var _mst_role = require("./mst_role");
var _mst_users = require("./mst_users");
var _tbl_approval_history = require("./tbl_approval_history");
var _tbl_auth_matrix_sjh = require("./tbl_auth_matrix_sjh");
var _tbl_manual_sjh = require("./tbl_manual_sjh");
var _tbl_manual_sjh_version = require("./tbl_manual_sjh_version");
var _tbl_master_halal = require("./tbl_master_halal");
var _tbl_master_halal_change_doc = require("./tbl_master_halal_change_doc");
var _tbl_task_list = require("./tbl_task_list");

function initModels(sequelize) {
  var mst_department = _mst_department(sequelize, DataTypes);
  var mst_role = _mst_role(sequelize, DataTypes);
  var mst_users = _mst_users(sequelize, DataTypes);
  var tbl_approval_history = _tbl_approval_history(sequelize, DataTypes);
  var tbl_auth_matrix_sjh = _tbl_auth_matrix_sjh(sequelize, DataTypes);
  var tbl_manual_sjh = _tbl_manual_sjh(sequelize, DataTypes);
  var tbl_manual_sjh_version = _tbl_manual_sjh_version(sequelize, DataTypes);
  var tbl_master_halal = _tbl_master_halal(sequelize, DataTypes);
  var tbl_master_halal_change_doc = _tbl_master_halal_change_doc(sequelize, DataTypes);
  var tbl_task_list = _tbl_task_list(sequelize, DataTypes);


  return {
    mst_department,
    mst_role,
    mst_users,
    tbl_approval_history,
    tbl_auth_matrix_sjh,
    tbl_manual_sjh,
    tbl_manual_sjh_version,
    tbl_master_halal,
    tbl_master_halal_change_doc,
    tbl_task_list,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
