const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tbl_master_halal', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    plant: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    material_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vendor_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    manufacturer: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    expired_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    material_desc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vendor_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    release_until: {
      type: DataTypes.DATE,
      allowNull: true
    },
    halal_body: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cert_number: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    isActive: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    category: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    packaging_file_path: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    attachment_file_path: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    halal_certificate_file_path: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tbl_master_halal',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
