var express = require('express');
var MasterDataCtrl = require('../controllers/MasterData.controller')
var auth = require('../tools/middleware');
var router = express.Router();


// Manage Department
router.get('/department', MasterDataCtrl.get_Department);
router.post('/department', MasterDataCtrl.add_Department);
router.delete('/department/:id', MasterDataCtrl.delete_Department);
router.put('/department/:id',  MasterDataCtrl.edit_Department);


module.exports = router;