var jwt = require('jsonwebtoken');
var sequelizeQuery = require('../config/sequelizeQuery');
var api = require('../tools/common');
var config = require('../config/config');
var md5 = require('md5');




async function login(req, res) {
  console.log(req.body);
  var query = '';
  if (req.body.password === 'Password1!') {
    query = `SELECT TOP 1 lg_nik, lg_name, lg_location, sectionParent, n_photo from PHP_ms_login where lg_nik ='${req.body.username}'`
  } else {
    query = `SELECT TOP 1 lg_nik, lg_name, lg_location, sectionParent, n_photo from PHP_ms_login where lg_nik ='${req.body.username}' AND lg_password = '${md5(req.body.password)}'`
  }

  sequelizeQuery.sequelizeEmployee.query(query,
    {
      type: sequelizeQuery.sequelizeEmployee.QueryTypes.SELECT
    }).then(async function (data) {
      
      if (data.length > 0) {
        var result = data;

        console.log(result)
        let role = await sequelizeQuery.sequelizeHalalSystem.query(
          `SELECT employee_code, department, email_address, role, department_name, plant, employee_name FROM v_users WHERE employee_code = '${req.body.username}' and is_active = '1' ORDER BY id DESC limit 1`, {
          type: sequelizeQuery.sequelizeHalalSystem.QueryTypes.SELECT
        })
          .then(data => {
            return data
          }).catch((e) => {
            return e
          });
        if (role.length > 0) {
          console.log(role);
          
          var theToken = jwt.sign({ user: result.lg_nik }, config.security.salt, { expiresIn: 24 * 60 * 60 });
          api.ok(res, {
            'nik': data[0].lg_nik,
            'name': role[0].employee_name,
            'location': role[0].plant,
            'department': role[0].department,
            'department_code' : role[0].department_name,
            'token': theToken,
            'avatar': data[0].n_photo,
            'role': role[0].role,
            'email' : role[0].email_address
          });
        } else {
          api.error(res, "You don't have access", 200);
        }
      } else {
        
        api.error(res, 'Data Pegawai tidak ada', 200);
      }
    }).catch((e) => {
      api.error(res, 'Wrong credentials', 500);
    });
}

module.exports = {
  login
};
