var models = require('../config/sequelizeORM')
var sequelizeQuery = require('../config/sequelizeQuery')
var api = require('../tools/common')
var formidable = require('formidable');
var moment = require('moment')
var btoa = require('btoa');
var mv = require('mv');
var path = require('path');
var md5 = require('md5');
const { lchown } = require('fs/promises');


// Master Department

function get_Department(req, res) {
  sequelizeQuery.sequelizeHalalSystem.query(
    `SELECT * FROM mst_department`, {
    type: sequelizeQuery.sequelizeHalalSystem.QueryTypes.SELECT
  }
  )
    .then(function (data) {
      if (data.length > 0) {
        api.ok(res, data)
      } else {
        api.error(res, 'Record not found', 200)
      }
    }).catch((e) => {
      api.error(res, e, 500)
    })
}


function add_Department(req, res) {

  req.body.created_date =  moment().format("YYYY-MM-DD H:mm:ss");
  models.mst_department.create(req.body, {
    raw: true
  }).then(function (create) {
    api.ok(res, create)
  }).catch((e) => {
    api.error(res, e, 500)
  })
}



function delete_Department(req, res) {
  models.mst_department.destroy({
    where: {
      Id: req.params.id
    }
  })
    .then(function (deletedRecord) {
      if (deletedRecord === 1) {
        api.ok(res, deletedRecord)
      } else {
        api.error(res, 'Record not found', 200);
      }
    }).catch((e) => {
      api.error(res, e, 500)
    })
}


function edit_Department(req, res) {
  models.mst_department.update(
    req.body, {
    where: {
      Id: req.params.id
    }
  })
    .then(data => {
      api.ok(res, req.body)
    }).catch((e) => {
      api.error(res, e, 500)
    })
}



module.exports = {
  get_Department,
  add_Department,
  delete_Department,
  edit_Department,

 
};